/*
 * spi_bang.h
 *
 *  Created on: Jul 22, 2014
 *      Author: gregpace
 */

#ifndef SPI_BANG_H_
#define SPI_BANG_H_

#include "nrf_gpio.h"

typedef struct {
 uint32_t cs;
 uint32_t miso;
 uint32_t mosi;
 uint32_t sck;
} spi_pins;

static spi_pins* spi_pins1;
/*
static spi_pins* spi_pins2;
static spi_pins* spi_pins3;
*/

void setup_spi1(spi_pins* set_spi_pins);

void spi1_mode3_BE_tx_rx(uint16_t length, uint8_t* send, uint8_t* recv);
void spi1_mode3_BE_tx(uint16_t length, uint8_t* send);
void spi1_mode3_BE_rx(uint16_t length, uint8_t* recv);
#endif /* SPI_BANG_H_ */
