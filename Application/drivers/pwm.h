/*
 * pwm.h
 *
 *  Created on: Jul 5, 2014
 *      Author: IEUSER
 */

#ifndef PWM_H_
#define PWM_H_

#include "application_definitions.h"
#include "nrf.h"
#include "nrf_gpiote.h"
#include "nrf_gpio.h"
#include "nrf_assert.h"
#include "nrf_soc.h"
#include "nrf_sdm.h"
#include "app_error.h"
#include "app_gpiote.h"

/* TODO make this driver actually have some width modulation capabilities
 *
 * Requirements to use this driver:
 * 1) high frequency clock (HFC) must be used.
 * 1.1) If not on, the HFC will be enabled when calling pwm_init( prams )
 * 2) Even though it says "pwm" all this driver really does is just send out a 1/2
 * 	  duty square wave on one of the gpio pins based on the provided frequency
 */

/* Sets the frequency of the pulse width modulation in hz
 * in increments of 50 hz. Try to have this function compile time execute.
 *
 * lowTime = (HFCLK / 2 ^ prescaler) * offset_low;
 * higTime = (HFCLK / 2 ^ prescaler) * offset_high;
 * *** make sure (offset_low + offset_high) is less than 2 ^ 16 (64k),
 *
 * Changes will not take effect until after start_pwm() is called
 */
void set_pwm_wave(uint8_t prescaler, uint16_t offset_1, uint16_t offset_2);

//initialize pwm, don't call inside in a isr.
void pwm_init();

//start the pwm
void start_pwm();
//stop the pwm
void stop_pwm( bool stop_hfclk);

//pin used for the pwm, doesn't take effect until after start_pwm is called
uint8_t pwm_pin;

//information needed to set the frequency,
//default frequency is ~2khz depending on what the actual HFC is (not what _cpu_clk is)
static uint8_t _prescaler = 5; 				//sets clock speed into timer to 2MHz
static uint16_t _prescaler_offset_1  = 125; 	//Make it such that every 125 clk cycles the gpio state changes
static uint16_t _prescaler_offset_2 = 125;

//Proprietary stuff to get the pwm going
//--> void TIMER2_IRQHandler( void ); is used but cannot be defined again, so this is just a refrence
static __INLINE void TIMER2_init();
static __INLINE void gpiote_init();
static __INLINE void ppi_init();
#endif /* PWM_H_ */
