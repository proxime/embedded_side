/*
 * spi_bang.c
 *
 *  Created on: Jul 22, 2014
 *      Author: gregpace
 */

#include "spi_bang.h"

void setup_spi1(spi_pins* set_spi_pins){
	spi_pins1 = set_spi_pins;
}


static void __INLINE spi_bang_mode3_BIGENDIAN(spi_pins* pins, uint16_t len, uint8_t* send, uint8_t* recv){
	//setup the pins
	nrf_gpio_cfg_output(pins->cs);
	nrf_gpio_cfg_output(pins->sck);
	nrf_gpio_cfg_output(pins->mosi);
	nrf_gpio_cfg_input(pins->miso, NRF_GPIO_PIN_NOPULL);

	//set cs as high, then enable
	nrf_gpio_pin_set(pins->cs);
	nrf_gpio_pin_set(pins->sck);
	nrf_gpio_pin_clear(pins->cs);

	int shift, cnt = 0;
	while(cnt < len)
	{
		shift = 7;
		//shift in the
		while(shift >= 0){

			//toggle clk
			nrf_gpio_pin_clear(pins->sck);
			//write to mosi line
			if(send != 0) nrf_gpio_pin_write(pins->mosi, send[cnt] >> shift & 1);

			//toggle clk
			nrf_gpio_pin_set(pins->sck);
			//capture what is on miso line
			if(recv != 0) recv[cnt] |= (nrf_gpio_pin_read(pins->miso) & 1) << shift;

			shift--;
		}
		cnt++;
	}

	nrf_gpio_pin_set(pins->cs);
}

void spi1_mode3_BE_tx_rx(uint16_t length, uint8_t* send, uint8_t* recv){
	spi_bang_mode3_BIGENDIAN(spi_pins1, length, send, recv);
}

void spi1_mode3_BE_tx(uint16_t length, uint8_t* send){
	spi_bang_mode3_BIGENDIAN(spi_pins1, length, send, 0);
}

void spi1_mode3_BE_rx(uint16_t length, uint8_t* recv){
	spi_bang_mode3_BIGENDIAN(spi_pins1, length, 0, recv);
}


