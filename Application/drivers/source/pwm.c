/*
 * pwm.c
 *
 *  Created on: Jul 5, 2014
 *      Author: IEUSER
 */
#include "pwm.h"

static uint8_t sd_enabled = 0;
static uint8_t accel_gpiote_channel_numb = PWM_GPIOTE_CHANNEL_NUMBER;

void set_pwm_wave(uint8_t prescaler, uint16_t offset_1, uint16_t offset_2)
{
	_prescaler = prescaler & 0x0F; 			//prescaler is from 0 to 0x0F
	_prescaler_offset_1 = offset_1; 	//offset can be from 0 to 0x7FFF
	_prescaler_offset_2 = offset_2;
}

void pwm_init() {
	sd_softdevice_is_enabled(&sd_enabled);
	TIMER2_init();
	gpiote_init();
	ppi_init();
}

static void start_hfclk_for_pwm(){
	if(sd_enabled == 0){
		if( NRF_CLOCK->HFCLKSTAT != CLOCK_HFCLKSTAT_STATE_Running << CLOCK_HFCLKSTAT_STATE_Pos){
			NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
			NRF_CLOCK->TASKS_HFCLKSTART = 1;
			NRF_CLOCK->TASKS_LFCLKSTOP = 1;

			/* Wait for the clock to start
			 * No need to use an interrupt to handle this event, it would probably take longer
			 * to have the OS deal with it
			 */
			while(NRF_CLOCK->EVENTS_HFCLKSTARTED == 0){ /*wait for HFC to start*/ }
		}
	} else {
		//go through the soft device to set up the high frequency clock
			sd_clock_hfclk_request();

			uint32_t crystl_is_running=0;
			while(!crystl_is_running){
				sd_clock_hfclk_is_running(&crystl_is_running);
			}
	}
}

static void stop_hfclk_for_pwm_end(){
	if(sd_enabled == 0){

	} else {
		sd_clock_hfclk_release();

		uint32_t crystl_is_running = 1;
		while(crystl_is_running){
			sd_clock_hfclk_is_running(&crystl_is_running);
		}
	}
}

void start_pwm()
{
	start_hfclk_for_pwm();
	PWM_TIMER->TASKS_CLEAR = 1;
	PWM_TIMER->TASKS_START = 1;
}

void stop_pwm(bool stop_hfclk)
{
	PWM_TIMER->TASKS_STOP = 1;

	//Toggle the voltage to low on the pin so that it doesn't drive current through the speaker
	if( nrf_gpio_pin_read(PWM_OUTPUT_PIN_NUMBER) == 1){
		NRF_GPIOTE->TASKS_OUT[accel_gpiote_channel_numb] = 1;
	}

	if(stop_hfclk){
		stop_hfclk_for_pwm_end();
	}
}

void TIMER2_init(){
	// Basic configuration to make timer peripheral a timer
	PWM_TIMER->MODE = TIMER_MODE_MODE_Timer << TIMER_MODE_MODE_Pos;
	PWM_TIMER->BITMODE = TIMER_BITMODE_BITMODE_16Bit << TIMER_BITMODE_BITMODE_Pos;
	PWM_TIMER->PRESCALER = _prescaler;

	/* setup the capture/compare register 0 as the compare register to send the first interrupt
	 * to the gpio
	 * See if the clock is 16Mhz or 32Mhz and adjust the prescaler accordingly
	 */
	uint8_t cpu_has_16MHz_clk = ((uint8_t) NRF_CLOCK->XTALFREQ) == CLOCK_XTALFREQ_XTALFREQ_16MHz << CLOCK_XTALFREQ_XTALFREQ_Pos;
	PWM_TIMER->CC[0] = cpu_has_16MHz_clk ? _prescaler_offset_1 : _prescaler_offset_1 << 1;

	/* setup the capture/compare register 1 as the compare register to send the second interrupt
	 * to the gpio
	 * See if the clock is 16Mhz or 32Mhz and adjust the prescaler accordingly
	 */
	uint16_t cc_high = _prescaler_offset_2 + _prescaler_offset_2;
	PWM_TIMER->CC[1]= cpu_has_16MHz_clk ? cc_high : cc_high << 1;

	/*setup the short to clear the timer on every task compare 2*/
	PWM_TIMER->SHORTS = TIMER_SHORTS_COMPARE1_CLEAR_Enabled << TIMER_SHORTS_COMPARE1_CLEAR_Pos;

	//make sure the timer has not started
	PWM_TIMER->TASKS_STOP = 1;
	PWM_TIMER->TASKS_CLEAR = 1;
}

void gpiote_init(){
	if(sd_enabled == 1){
		uint8_t err_code = app_gpiote_user_register(&accel_gpiote_channel_numb,
													0, //interrupt only happens from low to high
													0,
													0 //since no interrupt, then no need for interrupt handler
												);
	}
		nrf_gpio_cfg_output(PWM_OUTPUT_PIN_NUMBER);
		nrf_gpio_pin_clear(PWM_OUTPUT_PIN_NUMBER);
	#if PWM_INITIAL_VALUE == 1
		nrf_gpiote_task_config(accel_gpiote_channel_numb, PWM_OUTPUT_PIN_NUMBER, NRF_GPIOTE_POLARITY_TOGGLE, NRF_GPIOTE_INITIAL_VALUE_HIGH);
	#else
		nrf_gpiote_task_config(accel_gpiote_channel_numb, PWM_OUTPUT_PIN_NUMBER, NRF_GPIOTE_POLARITY_TOGGLE, NRF_GPIOTE_INITIAL_VALUE_LOW);
	#endif
}

void ppi_init(){
	if(sd_enabled == 0){
		//Set the timer TO gpiote channel up
		NRF_PPI->CH[PWM_PPI_FIRST_CHANNEL].EEP = (uint32_t)&PWM_TIMER->EVENTS_COMPARE[0];
		NRF_PPI->CH[PWM_PPI_FIRST_CHANNEL].TEP = (uint32_t)&NRF_GPIOTE->TASKS_OUT[PWM_GPIOTE_CHANNEL_NUMBER];

		NRF_PPI->CH[PWM_PPI_SECOND_CHANNEL].EEP = (uint32_t)&PWM_TIMER->EVENTS_COMPARE[1];
		NRF_PPI->CH[PWM_PPI_SECOND_CHANNEL].TEP = (uint32_t)&NRF_GPIOTE->TASKS_OUT[PWM_GPIOTE_CHANNEL_NUMBER];

		NRF_PPI->CHENSET |= 1UL << PWM_PPI_FIRST_CHANNEL |
							1UL << PWM_PPI_SECOND_CHANNEL ;
	} else {
		//use the soft device functions
		sd_ppi_channel_assign(
				PWM_PPI_FIRST_CHANNEL,
				(void*) &PWM_TIMER->EVENTS_COMPARE[0],
				(void*) &NRF_GPIOTE->TASKS_OUT[PWM_GPIOTE_CHANNEL_NUMBER]);

		sd_ppi_channel_assign(
				PWM_PPI_SECOND_CHANNEL,
				(void*) &PWM_TIMER->EVENTS_COMPARE[1],
				(void*) &NRF_GPIOTE->TASKS_OUT[PWM_GPIOTE_CHANNEL_NUMBER]);

		sd_ppi_channel_enable_set(1 << PWM_PPI_FIRST_CHANNEL | 1 << PWM_PPI_SECOND_CHANNEL);
	}
}
