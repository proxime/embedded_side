/*
 * bb_cfg.h
 *
 *  Created on: Jul 23, 2014
 *      Author: gregpace
 */

#ifndef BB_CFG_H_
#define BB_CFG_H_

/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 */

 /** @cond To make doxygen skip this file */

/** @file
 *
 * @defgroup ble_sdk_app_proximity_bondmngr_cfg Proximity Bond Manager Configuration
 * @{
 * @ingroup ble_sdk_app_proximity
 * @brief Definition of bond manager configurable parameters
 */

/**@brief Number of CCCDs used in the proximity application. */
#define BLE_BONDMNGR_CCCD_COUNT            1

/**@brief Maximum number of bonded centrals. */
#define BLE_BONDMNGR_MAX_BONDED_CENTRALS   7


/** @} */
/** @endcond */


#endif /* BB_CFG_H_ */
