#ifndef APPL_DEFN
#define APPL_DEFN

#include "nrf.h"
#include "boards.h"
#include "spi_master.h"
#include "nrf_gpio.h"
#include "nrf_gpiote.h"

/*
 * Non-bluetooth used PPI channels
 */
#define PWM_PPI_FIRST_CHANNEL 		0
#define PWM_PPI_SECOND_CHANNEL 		1

/*
 * GPIO pins/ports used
 */
#define PWM_OUTPUT_PIN_NUMBER 		1u
#define ACCEL_CS_PIN_NUMBER			6u
#define ACCEL_SPI_CLK_PIN_NUMBER 	4u
#define ACCEL_SPI_MO_PIN_NUMBER 	2u
#define ACCEL_SPI_MI_PIN_NUMBER 	0u
#define ACCEL_INT_PIN_NUMBER		7u

/*
 * GPIOTE channels
 * These get superseded if the softdevice is enabled
 */
#define PWM_GPIOTE_CHANNEL_NUMBER 	0
#define ACCEL_GPIOTE_CHANNEL_NUMBER 1ul

/*
 * Pulse width modulation specific macros
 * to set the frequency and duty to 2k at 50%
 */
#define PWM_TIMER					NRF_TIMER2
#define PWM_INITIAL_VALUE 			0 //define the first state of the gpio as Low
#define PWM_PRESCALER 				8
#define PWM_OFFSET_1 				135
#define PWM_OFFSET_2 				135

/*
 * Accelerometer specific macros
 */
//#define ACCEL_SPI 					SPI0
//#define ACCEL_SPI_MODE 				SPI_MODE3
//#define ACCEL_SPI_USE_BIG_ENDIAN 	1
#define ACCEL_INT_EDGE_POLARITY		NRF_GPIOTE_POLARITY_LOTOHI
#define ACCEL_INT_PULL_CONFIG		NRF_GPIO_PIN_NOPULL
//#define ACCEL_SPI_FRQUENCY			SPI_FREQ_125KBPS
#define ACCEL_THRESHOLD				0x16
#define ACCEL_INT_TIME				0x8

#endif //APPL_DEFN

