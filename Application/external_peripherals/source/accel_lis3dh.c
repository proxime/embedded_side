/*
 * accel_lis3dh.c
 *
 *  Created on: Jul 20, 2014
 *      Author: gregpace
 */

#include "accel_lis3dh.h"

//specificly needed for soft device
static  app_gpiote_user_id_t accel_gpiote_user_id;
static  uint8_t dropdetect_initialized = 0;
static  drop_detect_handler gpiote_handler = 0;

static void sd_accel_gpiote_event_handler(uint32_t event_pins_low_to_high, uint32_t event_pins_high_to_low)
{
	//this function needs to be implimented by the user
	gpiote_handler();

	//reset the accelerometer
	int8_t snd2[2]={0x80 | 0x31, 0};
	spi1_mode3_BE_tx(2, snd2);
}

uint8_t accel_lis3dh_dropdetect_init(
										Scale scale,
										Power_Mode pwMode,
										uint8_t g_threshold,
										uint8_t drop_time,
										drop_detect_handler evnt_handler
									)
{
	gpiote_handler = evnt_handler;
	pins.cs   = ACCEL_CS_PIN_NUMBER;
	pins.mosi = ACCEL_SPI_MO_PIN_NUMBER;
	pins.miso = ACCEL_SPI_MI_PIN_NUMBER;
	pins.sck  = ACCEL_SPI_CLK_PIN_NUMBER;
	setup_spi1(&pins);

	const uint8_t  READ_WRITE_Pos = 7; // (1 << READ_WRITE_Pos)
	const uint8_t ADDR_INCRM_Pos = 6;

	//for configuration registers
	struct{
			uint8_t WMaddr;
			uint8_t cfg[5];
	} ctrl_reg;

	//for setting the int_trigger conditions
	struct{
		uint8_t WMaddr;
		uint8_t threshold;
		uint8_t duration;
	}int_trigger_cond;

	//for configuring the interrupt into drop mode
	struct{
		uint8_t WMaddr;
		uint8_t cfg;
	} int_cfg;

	//for setting the drop_time
	/*
	 * Configure the first Config register
	 * - Set mode to desired pwMode
	 * - Keep all axis enabled
	 */
	switch( pwMode ){
		case PowerDownMode:
			ctrl_reg.cfg[0] = ACCEL_LIS3DH_DATA_RATE_SET_PwrDwn << ACCEL_LIS3DH_DATA_RATE_Pos;
			break;
		case LOWP_1HZ:
			ctrl_reg.cfg[0] =  ACCEL_LIS3DH_DATA_RATE_SET_1hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_10HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_10hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_25HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_25hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_50HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_50hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_100HZ:
			ctrl_reg.cfg[0] =  ACCEL_LIS3DH_DATA_RATE_SET_100hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_200HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_200hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_400HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_400hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_1p6KHZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_1p6khz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case LOWP_5KHZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_5khz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_LPOW_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_1HZ:
			ctrl_reg.cfg[0] =  ACCEL_LIS3DH_DATA_RATE_SET_1hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_10HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_10hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_25HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_25hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_50HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_50hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_100HZ:
			ctrl_reg.cfg[0] =  ACCEL_LIS3DH_DATA_RATE_SET_100hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_200HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_200hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NORM_400HZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_400hz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
		case NROM_1p25KHZ:
			ctrl_reg.cfg[0] = 	ACCEL_LIS3DH_DATA_RATE_SET_1p25khz << ACCEL_LIS3DH_DATA_RATE_Pos |
							ACCEL_LIS3DH_NORM_MODE_Set << ACCEL_LIS3DH_POW_MODE_Pos;
			break;
	}
	ctrl_reg.cfg[0] |= 	ACCEL_LIS3DH_X_AXIS_Enb << ACCEL_LIS3DH_X_AXIS_Pos |
						 	ACCEL_LIS3DH_Y_AXIS_Enb << ACCEL_LIS3DH_Y_AXIS_Pos |
						 	ACCEL_LIS3DH_Z_AXIS_Enb << ACCEL_LIS3DH_Z_AXIS_Pos;

	/* Configure the second register
	 * -disable the high-pass filter on ao1
	 */
	ctrl_reg.cfg[1] =	ACCEL_LIS3DH_ENB_AOI_INT1_HPSS_FLTR_Clr << ACCEL_LIS3DH_ENB_AOI_INT1_HPSS_FLTR_Pos;

	/* Configure the third register
	 * -Enable the Int1, disable all others
	 */
	ctrl_reg.cfg[2] =		ACCEL_LIS3DH_AOI1_INT1_ENB_Set << ACCEL_LIS3DH_AOI1_INT1_ENB_Pos;

	/* Configure the fourth register
	 * -Set the scale to the specified time,
	 * -keep as big endian
	 * -no self test
	 * -disable high resolution
	 * -keep spi 4 wire interface (but could be changed to 3)
	 */
	ctrl_reg.cfg[3] =		scale << ACCEL_LIS3DH_SCALE_SELECTION_Pos;

	/* Configure the fith register
	 * -No rebooting memory
	 * -No FIFO
	 * -Latch the interrupt request (instead of having it realtime)
	 * -disable 4D
	 */
	ctrl_reg.cfg[4] =		ACCEL_LIS3DH_LATCH_INT1_Set << ACCEL_LIS3DH_LATCH_INT1_Pos;

	ctrl_reg.WMaddr =		0 << READ_WRITE_Pos |
							1 << ADDR_INCRM_Pos |
							ACCEL_LIS3DH_CTRL_REG1_Adr;

	int_trigger_cond.threshold = g_threshold & ACCEL_LIS3DH_THRESHOLD_Msk;
	int_trigger_cond.duration  = drop_time & ACCEL_LIS3DH_DURATION_Msk;
	int_trigger_cond.WMaddr	   = 0 << READ_WRITE_Pos |
								 1 << ADDR_INCRM_Pos |
								 ACCEL_LIS3DH_THRESHOLD_Adr;

	/* Set up interrupt 1 for
	 * -drop mode (and combination of interrupts)
	 * -interrupts for all axis on +/- sides
	 */
	int_cfg.cfg = 	ACCEL_LIS3DH_INT1_CFG_MODE_ANDComb 	<< ACCEL_LIS3DH_INT1_CFG_MODE_Pos 	 |
					//ACCEL_LIS3DH_INT1_CFG_Z_H_ENB_Set 	<< ACCEL_LIS3DH_INT1_CFG_Z_H_ENB_Pos |
					ACCEL_LIS3DH_INT1_CFG_Z_L_ENB_Set 	<< ACCEL_LIS3DH_INT1_CFG_Z_L_ENB_Pos |
					//ACCEL_LIS3DH_INT1_CFG_Y_H_ENB_Set 	<< ACCEL_LIS3DH_INT1_CFG_Y_H_ENB_Pos |
					ACCEL_LIS3DH_INT1_CFG_Y_L_ENB_Set 	<< ACCEL_LIS3DH_INT1_CFG_Y_L_ENB_Pos |
					//ACCEL_LIS3DH_INT1_CFG_X_H_ENB_Set 	<< ACCEL_LIS3DH_INT1_CFG_X_H_ENB_Pos |
					ACCEL_LIS3DH_INT1_CFG_X_L_ENB_Set 	<< ACCEL_LIS3DH_INT1_CFG_X_L_ENB_Pos ;
	int_cfg.WMaddr = 0 << READ_WRITE_Pos |
			 	 	 0 << ADDR_INCRM_Pos |
			 	 	 ACCEL_LIS3DH_INT1_CFG_Adr;

	/*Setup the interrupt on the specified pin*/
	nrf_gpio_cfg_input( ACCEL_INT_PIN_NUMBER, ACCEL_INT_PULL_CONFIG);
	nrf_gpiote_event_config( ACCEL_GPIOTE_CHANNEL_NUMBER, ACCEL_INT_PIN_NUMBER, ACCEL_INT_EDGE_POLARITY);

	uint8_t sd_enabled;
	uint8_t err_code;

	err_code = sd_softdevice_is_enabled(&sd_enabled);
	APP_ERROR_CHECK(err_code);

	dropdetect_initialized = 1;

	//use the app_gpiote to set the interrupt on the pin
	err_code = app_gpiote_user_register(&accel_gpiote_user_id,
								1 << ACCEL_INT_PIN_NUMBER, //interrupt only happens from low to high
								0,
								sd_accel_gpiote_event_handler
							);
	if (err_code != NRF_SUCCESS)
	{
		return err_code;
	}

	//make sure that the gpiote is disabled before triggering these.
	sd_enabled ? sd_nvic_DisableIRQ(GPIOTE_IRQn) : NVIC_DisableIRQ(GPIOTE_IRQn);

	//Write to the accelerometer module, disable interrupts when this is done
	spi1_mode3_BE_tx(6, (uint8_t*) &ctrl_reg);
	spi1_mode3_BE_tx(3, (uint8_t*) &int_trigger_cond);
	spi1_mode3_BE_tx(2, (uint8_t*) &int_cfg);

	//clear any left over interrupts
	int8_t snd2[2]={0x80 | 0x31, 0};
	spi1_mode3_BE_tx(2, snd2);

	sd_enabled ? sd_nvic_ClearPendingIRQ(GPIOTE_IRQn) : NVIC_ClearPendingIRQ(GPIOTE_IRQn);
	sd_enabled ? sd_nvic_SetPriority(GPIOTE_IRQn,APP_IRQ_PRIORITY_LOW) : NVIC_SetPriority(GPIOTE_IRQn,APP_IRQ_PRIORITY_LOW);
	sd_enabled ? sd_nvic_EnableIRQ(GPIOTE_IRQn) : NVIC_EnableIRQ(GPIOTE_IRQn);

	return NRF_SUCCESS;
}

uint8_t accel_lis3dh_enable(){
	if(dropdetect_initialized){
		//clear any left over interrupts that may have happened
		int8_t snd2[2]={0x80 | 0x31, 0};
		spi1_mode3_BE_tx(2, snd2);

		return app_gpiote_user_enable(accel_gpiote_user_id);
	} else {
		return NRF_ERROR_INVALID_STATE;
	}
}
