/*
 * accel_lis3dh.h
 *
 *  Created on: Jul 20, 2014
 *      Author: gregpace
 */

#ifndef ACCEL_LIS3DH_H_
#define ACCEL_LIS3DH_H_

#include "spi_bang.h"
#include "accel_lis3dh_bitfields.h"
#include "application_definitions.h"
#include "nrf_gpio.h"
#include "nrf_gpiote.h"
#include "app_gpiote.h"
#include "nrf_sdm.h"


typedef enum{
	SCALE_2G  = 0,
	SCALE_4G  = 1,
	SCALE_8G  = 2,
	SCALE_16G = 3,
}Scale;

typedef enum{
	PowerDownMode = 0,

	LOWP_1HZ,
	LOWP_10HZ,
	LOWP_25HZ,
	LOWP_50HZ,
	LOWP_100HZ,
	LOWP_200HZ,
	LOWP_400HZ,
	LOWP_1p6KHZ,
	LOWP_5KHZ,

	NORM_1HZ,
	NORM_10HZ,
	NORM_25HZ,
	NORM_50HZ,
	NORM_100HZ,
	NORM_200HZ,
	NORM_400HZ,
	NROM_1p25KHZ,
}Power_Mode;

typedef void (*drop_detect_handler) (void);

static 	spi_pins pins;

uint8_t accel_lis3dh_dropdetect_init(
		Scale scale,
		Power_Mode pwMode,
		uint8_t g_threshold,
		uint8_t drop_time,
		drop_detect_handler evnt_handler
		);

//only call this if the soft device is used. otherwise it will always return NRF_ERROR_INVALID_STATE (which means nothing without it)
uint8_t accel_lis3dh_enable();
#endif /* ACCEL_LIS3DH_H_ */
