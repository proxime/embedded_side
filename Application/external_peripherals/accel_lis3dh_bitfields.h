/*
 * accel_lis3dh.h
 *
 *  Created on: Jul 20, 2014
 *      Author: gregpace
 */

#ifndef ACCEL_LIS3DH_BITFIELDS_H_
#define ACCEL_LIS3DH_BITFIELDS_H_

//Bitfields for the accelerometer

/* status register
 * used to show the status of the fifo for 1 (x), 2 (y), and 3 (z) axis FIFOs
 */
#define ACCEL_LIS3DH_STATUS_REG_AUX_Adr 	0x07u
#define ACCEL_LIS3DH_321_DATA_OVERUN_Msk	0x80u
#define ACCEL_LIS3DH_3_DATA_OVERUN_Msk 		0x40u
#define ACCEL_LIS3DH_2_DATA_OVERUN_Msk		0x20u
#define ACCEL_LIS3DH_1_DATA_OVERUN_Msk		0x10u
#define ACCEL_LIS3DH_321_DATA_AVAIL_Msk		0x08u
#define ACCEL_LIS3DH_3_DATA_AVAIL_Msk		0x04u
#define ACCEL_LIS3DH_2_DATA_AVAIL_Msk		0x02u
#define ACCEL_LIS3DH_1_DATA_AVAIL_Msk		0x01u

/* -Output read registers for each ADC and Temp sensor
 * -Grouped by high and low
 * -Remember! ADC 3 and Temp sensor are multiplexed, use
 * 	temp config register to switch between the two
 */
#define ACCEL_LIS3DH_1_ADC_OUT_L_Adr		0x08u
#define ACCEL_LIS3DH_1_ADC_OUT_H_Adr		0x09u
#define ACCEL_LIS3DH_2_ADC_OUT_L_Adr		0x0Au
#define ACCEL_LIS3DH_2_ADC_OUT_H_Adr		0x0Bu
#define ACCEL_LIS3DH_3_ADC_OUT_L_Adr		0x0Cu
#define ACCEL_LIS3DH_3_ADC_OUT_H_Adr		0x0Du

#define ACCEL_LIS3DH_TEMP_SENSOR_L_Adr		0x0Cu
#define ACCEL_LIS3DH_TEMP_SENSOR_H_Adr		0x0Du

/* -Int counter register
 * (I have no idea what this is for)
 */
#define ACCEL_LIS3DH_INT_COUNTER_Adr		0x0Eu

/* -Who-am-I register
 */
#define ACCEL_LIS3DH_WHO_AM_I_Adr			0x0Fu

/* -Temp configuration register to allow multiplexing
 * 	between ADC 3 and Temp sensor
 * 	TODO see if this register enables/disables ALL ADC or just ADC 3
 */
#define ACCEL_LIS3DH_TEMP_CFG_Adr			0x1Fu
#define ACCEL_LIS3DH_ADC_CTRL_Enb			1u
#define ACCEL_LIS3DH_ADC_CTRL_Dsbl			0u
#define ACCEL_LIS3DH_ADC_CTRL_Pos			7u
#define ACCEL_LIS3DH_TEMP_CTRL_Enb			1u
#define ACCEL_LIS3DH_TEMP_CTRL_Dsbl			0u
#define ACCEL_LIS3DH_TEMP_CTRL_Pos			6u

/* -Control register1 information
 */
#define ACCEL_LIS3DH_CTRL_REG1_Adr			0x20u

	/* -Control of the data rate
	 */
	#define ACCEL_LIS3DH_DATA_RATE_SET_PwrDwn	 	0x0u
	#define ACCEL_LIS3DH_DATA_RATE_SET_1hz			0x1u
	#define ACCEL_LIS3DH_DATA_RATE_SET_10hz			0x2u
	#define ACCEL_LIS3DH_DATA_RATE_SET_25hz			0x3u
	#define ACCEL_LIS3DH_DATA_RATE_SET_50hz			0x4u
	#define ACCEL_LIS3DH_DATA_RATE_SET_100hz		0x5u
	#define ACCEL_LIS3DH_DATA_RATE_SET_200hz		0x6u
	#define ACCEL_LIS3DH_DATA_RATE_SET_400hz		0x7u
	#define ACCEL_LIS3DH_DATA_RATE_SET_1p6khz		0x8u //low power mode only
	#define ACCEL_LIS3DH_DATA_RATE_SET_1p25khz 		0x9u	//only for normal mode
	#define ACCEL_LIS3DH_DATA_RATE_SET_5khz			0x9u //only for low power mode
	#define ACCEL_LIS3DH_DATA_RATE_Pos				4u

	/* -Control of power
	 */
	#define ACCEL_LIS3DH_LPOW_MODE_Set				1u
	#define ACCEL_LIS3DH_NORM_MODE_Set				0u
	#define ACCEL_LIS3DH_POW_MODE_Pos				3u

	/* -Individual enables for each axis
	 */
	#define ACCEL_LIS3DH_X_AXIS_Enb					1u
	#define ACCEL_LIS3DH_X_AXIS_Dis					0u
	#define ACCEL_LIS3DH_X_AXIS_Pos					0u

	#define ACCEL_LIS3DH_Y_AXIS_Enb					1u
	#define ACCEL_LIS3DH_Y_AXIS_Dis					0u
	#define ACCEL_LIS3DH_Y_AXIS_Pos					1u

	#define ACCEL_LIS3DH_Z_AXIS_Enb					1u
	#define ACCEL_LIS3DH_Z_AXIS_Dis					0u
	#define ACCEL_LIS3DH_Z_AXIS_Pos					2u

/* -Control register 2 information
 */
#define ACCEL_LIS3DH_CTRL_REG2_Adr		0x21u

	#define ACCEL_LIS3DH_HPSS_FLTR_MODE_NormMode		0u
	#define ACCEL_LIS3DH_HPSS_FLTR_MODE_Refrence		1u
	#define ACCEL_LIS3DH_HPSS_FLTR_MODE_AtoRstInt		3u
	#define ACCEL_LIS3DH_HPSS_FLTR_MODE_Pos				6u

	#define ACCEL_LIS3DH_HPSS_FLTR_CTOFF_FRQ_1			0u
	#define ACCEL_LIS3DH_HPSS_FLTR_CTOFF_FRQ_2			1u
	#define ACCEL_LIS3DH_HPSS_FLTR_CTOFF_FRQ_3			2u
	#define ACCEL_LIS3DH_HPSS_FLTR_CTOFF_FRQ_4			3u
	#define ACCEL_LIS3DH_HPSS_FLTR_CTOFF_FRQ_Pos		4u

	#define ACCEL_LIS3DH_FDS_ENB_Set					1u
	#define ACCEL_LIS3DH_FDS_ENB_Clr					0u
	#define ACCEL_LIS3DH_FDS_ENB_Pos					3u

	#define ACCEL_LIS3DH_ENB_CLICK_HPSS_FLTR_Set		1u
	#define ACCEL_LIS3DH_ENB_CLICK_HPSS_FLTR_Clr		0u
	#define ACCEL_LIS3DH_ENB_CLICK_HPSS_FLTR_Pos		2u

	#define ACCEL_LIS3DH_ENB_AOI_INT1_HPSS_FLTR_Set		1u
	#define ACCEL_LIS3DH_ENB_AOI_INT1_HPSS_FLTR_Clr		0u
	#define ACCEL_LIS3DH_ENB_AOI_INT1_HPSS_FLTR_Pos		0u

	#define ACCEL_LIS3DH_ENB_AOI_INT2_HPSS_FLTR_Set		1u
	#define ACCEL_LIS3DH_ENB_AOI_INT2_HPSS_FLTR_Clr		0u
	#define ACCEL_LIS3DH_ENB_AOI_INT2_HPSS_FLTR_Pos		1u

/* -Control Register 3 information
 */
#define ACCEL_LIS3DH_CTRL_REG3_Adr	0x22u

	#define ACCEL_LIS3DH_CLICK_INT1_ENB_Set				1u
	#define ACCEL_LIS3DH_CLICK_INT1_ENB_Clr				0u
	#define ACCEL_LIS3DH_CLICK_INT1_ENB_Pos				7u

	#define ACCEL_LIS3DH_AOI1_INT1_ENB_Set				1u
	#define ACCEL_LIS3DH_AOI1_INT1_ENB_Clr				0u
	#define ACCEL_LIS3DH_AOI1_INT1_ENB_Pos				6u

	#define ACCEL_LIS3DH_AOI2_INT1_ENB_Set				1u
	#define ACCEL_LIS3DH_AOI2_INT1_ENB_Clr				0u
	#define ACCEL_LIS3DH_AOI2_INT1_ENB_Pos				5u

	#define ACCEL_LIS3DH_DATARDY1_INT1_ENB_Set			1u
	#define ACCEL_LIS3DH_DATARDY1_INT1_ENB_Clr			0u
	#define ACCEL_LIS3DH_DATARDY1_INT1_ENB_Pos			4u

	#define ACCEL_LIS3DH_DATARDY2_INT1_ENB_Set			1u
	#define ACCEL_LIS3DH_DATARDY2_INT1_ENB_Clr			0u
	#define ACCEL_LIS3DH_DATARDY2_INT1_ENB_Pos			3u

	#define ACCEL_LIS3DH_WATRMK_INT1_ENB_Set			1u
	#define ACCEL_LIS3DH_WATRMK_INT1_ENB_Clr			0u
	#define ACCEL_LIS3DH_WATRMK_INT1_ENB_Pos			2u

	#define ACCEL_LIS3DH_DATAOVERRUN_INT1_ENB_Set		1u
	#define ACCEL_LIS3DH_DATAOVERRUN_INT1_ENB_Clr		0u
	#define ACCEL_LIS3DH_DATAOVERRUN_INT1_ENB_Pos		1u

/* -Control Register 4 info
 */

#define ACCEL_LIS3DH_CTRL_REG4_Adr	0x23u

	#define ACCEL_LIS3DH_BLOCK_DATA_UPDATE_Set		1u
	#define ACCEL_LIS3DH_BLOCK_DATA_UPDATE_Clr		0u
	#define ACCEL_LIS3DH_BLOCK_DATA_UPDATE_Pos		7u

	#define ACCEL_LIS3DH_BIG_ENDIAN_Set				1u
	#define ACCEL_LIS3DH_LITTLE_ENDIAN_Set			0u
	#define ACCEL_LIS3DH_ENDIAN_Pos					6u

	#define ACCEL_LIS3DH_SCALE_SELECTION_2g			0u
	#define ACCEL_LIS3DH_SCALE_SELECTION_4g			1u
	#define ACCEL_LIS3DH_SCALE_SELECTION_8g			2u
	#define ACCEL_LIS3DH_SCALE_SELECTION_16g		3u
	#define ACCEL_LIS3DH_SCALE_SELECTION_Pos		4u

	#define ACCEL_LIS3DH_HIGH_RESOLUTION_OUTPUT_Set	1u
	#define ACCEL_LIS3DH_HIGH_RESOLUTION_OUTPUT_Clr	1u
	#define ACCEL_LIS3DH_HIGH_RESOLUTION_OUTPUT_Pos	3u

	#define ACCEL_LIS3DH_SELF_TEST_Disable			0u
	#define ACCEL_LIS3DH_SELF_TEST_0				1u
	#define ACCEL_LIS3DH_SELF_TEST_1				2u
	#define ACCEL_LIS3DH_SELF_TEST_Pos				1u

	#define ACCEL_LIS3DH_SPI_MODE_Set				1u
	#define ACCEL_LIS3DH_SPI_MODE_Clr				0u
	#define ACCEL_LIS3DH_SPI_MODE_Pos				0u

/* -Control Register 5 info
 */
#define ACCEL_LIS3DH_CTRL_REG5_Adr	0x24u

	#define ACCEL_LIS3DH_REBOOT_MEMORY_CONTENT_Set		1u
	#define ACCEL_LIS3DH_REBOOT_MEMORY_CONTENT_Clr		0u
	#define ACCEL_LIS3DH_REBOOT_MEMORY_CONTENT_Pos		7u

	#define ACCEL_LIS3DH_REBOOT_FIFO_ENB_Set			1u
	#define ACCEL_LIS3DH_REBOOT_FIFO_ENB_Clr			0u
	#define ACCEL_LIS3DH_REBOOT_FIFO_ENB_Pos			6u

	#define ACCEL_LIS3DH_LATCH_INT1_Set					1u
	#define ACCEL_LIS3DH_LATCH_INT1_Clr					0u
	#define ACCEL_LIS3DH_LATCH_INT1_Pos					3u

	#define ACCEL_LIS3DH_4D_ENB_Set						1u
	#define ACCEL_LIS3DH_4D_ENB_Clr						0u
	#define ACCEL_LIS3DH_4D_ENB_Pos						2u

/* -Control Register 6
 * Just skip this one idk what it does
 */

/* -REFRENCE
 * -Maybe used for reading from to trigger the high pass filter
 */
#define ACCEL_LIS3DH_REFRENCE_Adr 0x26u

/* -Status Register
 *
 */
#define ACCEL_LIS3DH_STATUS_REG_Adr 0x27u

	#define ACCEL_LIS3DH_XYZ_OVERUN_Msk					0x80u
	#define ACCEL_LIS3DH_Z_OVERUN_Msk					0x40u
	#define ACCEL_LIS3DH_Y_OVERUN_Msk					0x20u
	#define ACCEL_LIS3DH_X_OVERUN_Msk					0x10u
	#define ACCEL_LIS3DH_XYZ_NEWDATA_Msk				0x08u
	#define ACCEL_LIS3DH_Z_NEWDATA_Msk					0x04u
	#define ACCEL_LIS3DH_Y_NEWDATA_Msk					0x02u
	#define ACCEL_LIS3DH_X_NEWDATA_Msk					0x01u

/* Out registers
 *
 */
#define ACCEL_LIS3DH_X_L_OUT_Adr		0x28u
#define ACCEL_LIS3DH_X_H_OUT_Adr		0x29u
#define ACCEL_LIS3DH_Y_L_OUT_Adr		0x2Au
#define ACCEL_LIS3DH_Y_H_OUT_Adr		0x2Bu
#define ACCEL_LIS3DH_Z_L_OUT_Adr		0x2Cu
#define ACCEL_LIS3DH_Z_H_OUT_Adr		0x2Du

// FIFO control register
#define ACCEL_LIS3DH_FIFO_CTRL_REG_Adr	0x2Eu

	#define ACCEL_LIS3DH_FIFO_CTRL_MODE_Bypass		0u
	#define ACCEL_LIS3DH_FIFO_CTRL_MODE_Fifo		1u
	#define ACCEL_LIS3DH_FIFO_CTRL_MODE_Stream		2u
	#define ACCEL_LIS3DH_FIFO_CTRL_MODE_Trigger		3u
	#define ACCEL_LIS3DH_FIFO_CTRL_MODE_Pos			6u

	#define ACCEL_LIS3DH_FIFO_CTRL_TRIGGER_INT1_Set	0u
	#define ACCEL_LIS3DH_FIFO_CTRL_TRIGGER_INT2_Set	1u
	#define ACCEL_LIS3DH_FIFO_CTRL_TRIGGER_SEL_Pos	5u

	#define ACCEL_LIS3DH_FIFO_CTRL_FTH_Pos				0u

//FIFO status register
#define ACCEL_LIS3DH_FIFO_SOURCE_REG	0X2Fu

	#define ACCEL_LIS3DH_FIFO_SOURCE_WATERMARK_Msk	0x80u
	#define ACCEL_LIS3DH_FIFO_SOURCE_OVERRUN_Msk	0x40u
	#define ACCEL_LIS3DH_FIFO_SOURCE_FSS_Msk		0x1Fu

//Int 1 configuration register
#define ACCEL_LIS3DH_INT1_CFG_Adr	0x30u

	#define ACCEL_LIS3DH_INT1_CFG_MODE_ORComb	0u
	#define ACCEL_LIS3DH_INT1_CFG_MODE_6DMov	1u
	#define ACCEL_LIS3DH_INT1_CFG_MODE_ANDComb	2u
	#define ACCEL_LIS3DH_INT1_CFG_MODE_6DPosit	3u
	#define ACCEL_LIS3DH_INT1_CFG_MODE_Pos		6u

	#define ACCEL_LIS3DH_INT1_CFG_Z_H_ENB_Set	1u
	#define ACCEL_LIS3DH_INT1_CFG_Z_H_ENB_Clr	0u
	#define ACCEL_LIS3DH_INT1_CFG_Z_H_ENB_Pos	5u

	#define ACCEL_LIS3DH_INT1_CFG_Z_L_ENB_Set	1u
	#define ACCEL_LIS3DH_INT1_CFG_Z_L_ENB_Clr	0u
	#define ACCEL_LIS3DH_INT1_CFG_Z_L_ENB_Pos	4u

	#define ACCEL_LIS3DH_INT1_CFG_Y_H_ENB_Set	1u
	#define ACCEL_LIS3DH_INT1_CFG_Y_H_ENB_Clr	0u
	#define ACCEL_LIS3DH_INT1_CFG_Y_H_ENB_Pos	3u

	#define ACCEL_LIS3DH_INT1_CFG_Y_L_ENB_Set	1u
	#define ACCEL_LIS3DH_INT1_CFG_Y_L_ENB_Clr	0u
	#define ACCEL_LIS3DH_INT1_CFG_Y_L_ENB_Pos	2u

	#define ACCEL_LIS3DH_INT1_CFG_X_H_ENB_Set	1u
	#define ACCEL_LIS3DH_INT1_CFG_X_H_ENB_Clr	0u
	#define ACCEL_LIS3DH_INT1_CFG_X_H_ENB_Pos	1u

	#define ACCEL_LIS3DH_INT1_CFG_X_L_ENB_Set	1u
	#define ACCEL_LIS3DH_INT1_CFG_X_L_ENB_Clr	0u
	#define ACCEL_LIS3DH_INT1_CFG_X_L_ENB_Pos	0u

//Sourch of the interrupt 1
#define ACCEL_LIS3DH_INT1_SOURCE_REG_Adr	0x31u

	#define ACCEL_LIS3DH_INT1_SOURCE_ACTIVE_Msk		0x40u
	#define ACCEL_LIS3DH_INT1_SOURCE_Z_HIGH_Msk		0x20u
	#define ACCEL_LIS3DH_INT1_SOURCE_Z_LOW_Msk		0x10u
	#define ACCEL_LIS3DH_INT1_SOURCE_Y_HIGH_Msk		0x08u
	#define ACCEL_LIS3DH_INT1_SOURCE_Y_LOW_Msk		0x04u
	#define ACCEL_LIS3DH_INT1_SOURCE_X_HIGH_Msk		0x02u
	#define ACCEL_LIS3DH_INT1_SOURCE_X_LOW_Msk		0x01u

//Threshold for each (z high, z low, y high, ect.) to generate an internal interrupt
#define ACCEL_LIS3DH_THRESHOLD_Adr		0x32u
#define ACCEL_LIS3DH_THRESHOLD_Msk		0x7Fu

//How long the interrupt condition must be held for
//See LIS3DH application note for details
#define ACCEL_LIS3DH_DURATION_Adr		0x33u
#define ACCEL_LIS3DH_DURATION_Msk		0x7Fu

#endif /* ACCEL_LIS3DH_BITFIELDS_H_ */
