################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../_build/startup/system_nrf51.c 

S_UPPER_SRCS += \
../_build/startup/gcc_startup_nrf51.S 

OBJS += \
./_build/startup/gcc_startup_nrf51.o \
./_build/startup/system_nrf51.o 

S_UPPER_DEPS += \
./_build/startup/gcc_startup_nrf51.d 

C_DEPS += \
./_build/startup/system_nrf51.d 


# Each subdirectory must supply rules for building sources it contributes
_build/startup/%.o: ../_build/startup/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -O0  -g3 -x assembler-with-cpp -DNRF51 -DDEBUG -DNRF51822_QFAA_GC -DDEBUG_NRF_USER -DBLE_STACK_SUPPORT_REQD -DBOARD_PCA10001 -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

_build/startup/%.o: ../_build/startup/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -O0  -g3 -DNRF51 -DDEBUG -DNRF51822_QFAA_GC -DDEBUG_NRF_USER -DBLE_STACK_SUPPORT_REQD -DBOARD_PCA10001 -DDEBUG -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/_build" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/Application" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/_build/linkers" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/_build/startup" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/Application/common" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/Application/drivers" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/Application/bluetooth" -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/app_common -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/ble -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/boards -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/esb -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/ext_sensors -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/gcc -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/gzll -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/gzp -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/s110 -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/sd_common -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/sdk -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/ble/rpc -I/Users/gregpace/Desktop/CMPE_FP/nrf51_sdk_v5_2_0_39364/nrf51822/Include/ble/ble_services -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/Application/external_peripherals" -I"/Users/gregpace/Desktop/CMPE_FP/workspace/proxime/Application/external_peripherals/source" -I/Users/gregpace/Desktop/CMPE_FP/gcc-arm-none-eabi-4_8-2014q2/lib/gcc/arm-none-eabi/4.8.4/include -std=gnu99 -O0 -g -fno-inline -Wno-unused-local-typedefs -mabi=aapcs -mfloat-abi=soft -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


